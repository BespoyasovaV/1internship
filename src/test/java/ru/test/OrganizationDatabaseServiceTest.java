package ru.test;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import ru.persons.single.OrganizationService;

import javax.xml.bind.JAXBException;

class OrganizationDatabaseServiceTest {

    @Test
    void getOrganizationSingle() {
        OrganizationService o1 = OrganizationService.getOrganizationSingle();
        OrganizationService o2 = OrganizationService.getOrganizationSingle();
        Assert.assertTrue(o1.equals(o2));
    }

    @Test
    void getOrganization () {
        Assert.assertNotNull(OrganizationService.getOrganization());
    }

}