package ru.test;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import ru.persons.single.PersonService;

import javax.xml.bind.JAXBException;

class PersonDataBaseServiceTest {


    @Test
    void getPersonSingle() {
        PersonService p1 = PersonService.getPersonSingle();
        PersonService p2 = PersonService.getPersonSingle();
        Assert.assertTrue(p1.equals(p2));
    }

    @Test
    void getPerson() {
        Assert.assertNotNull(PersonService.getPerson());

    }
}