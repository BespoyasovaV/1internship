package ru.test;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import ru.enums.Delivery;
import ru.exceptions.DocumentExistException;
import ru.generate.ProgramCollections;
import ru.generate.RandomValueService;
import ru.persons.single.PersonService;

class RandomValueServiceTest {

    @Test
    void generateRandomNumber() throws DocumentExistException {
        Assert.assertNotNull(RandomValueService.generateRandomNumber(5));
    }

    @Test
    void generateListMeaning() {
        Assert.assertTrue(RandomValueService.generateListMeaning(ProgramCollections.wordListOne,0).equals
                ("Доменная интеграция"));
    }

    @Test
    void generateEnum() {
       Assert.assertNotNull(RandomValueService.generateEnum(Delivery.values()));
    }

    @Test
    void generateDate() {
        Assert.assertNotNull(RandomValueService.generateDate());
    }

    @Test
    void getPersonFromList()  {
       Assert.assertNotNull(RandomValueService.getPersonFromList(PersonService.getPerson()));
    }
}