package ru.test;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import ru.persons.single.DepartmentService;

import javax.xml.bind.JAXBException;

class DepartmentServiceTest {

    @Test
    void getDepartmentSingle() {
        DepartmentService d1=DepartmentService.getDepartmentSingle();
        DepartmentService d2=DepartmentService.getDepartmentSingle();
        Assert.assertTrue(d1.equals(d2));
    }

    @Test
    void getDepartment(){
       Assert.assertNotNull(DepartmentService.getDepartment());
    }

}