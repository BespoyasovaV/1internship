<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>

<html>
<head>
    <link rel="stylesheet" href="recourses/documents.css">
    <title>java</title>
</head>
<body>
<h3>Таблица документов</h3>
<table border="3">
    <tr>
        <td class="fat">Name</td>
    </tr>
    <c:forEach items="${documentList}" var="documentList">
        <tr>
            <td>
                <a href="http://localhost:8080/task/documentFields?documentId=${documentList['id']}"> ${documentList.name}</a>
            </td>
        </tr>
    </c:forEach>
</table>
<% String prvurl = request.getHeader("Referer");%>
<a href="<%=prvurl%>">Вернуться назад</a>
</body>
</html>