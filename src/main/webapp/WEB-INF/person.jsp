<%@ page import="java.util.UUID" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>

<html>
<head>
    <link rel="stylesheet" href="recourses/person.css">
    <title>java</title>
</head>
<body>
<h3>Таблица сотрудников</h3>
<table border="3">
    <tr>
        <td class="fat">ID</td>
        <td class="fat">Name</td>
        <td class="fat">Surname</td>
        <td class="fat">Patronymic</td>
    </tr>
    <c:forEach items="${personList}" var="personList">
        <tr>
            <td>
                <a href="http://localhost:8080/task/documents?userId=${personList['id']}"> ${personList.id}</a>
            </td>
            <td> ${personList.name} </td>
            <td> ${personList.surname} </td>
            <td> ${personList.patronymic} </td>
        </tr>
    </c:forEach>
</table>
<% String prvurl = request.getHeader("Referer");%>
<div><a href="<%=prvurl%>" >Вернуться назад</a></div>
</body>
</html>
