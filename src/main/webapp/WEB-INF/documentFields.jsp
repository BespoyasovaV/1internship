<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>

<html>
<head>
    <link rel="stylesheet" href="recourses/fields.css">
    <title>java</title>
</head>
<body>
<h3>Перечень документов</h3>
<ol>
    <li>Название документа: <%=request.getAttribute("name")%></li>
    <li>Текст документа: <%=request.getAttribute("text")%></li>
    <li>Регистрационный номер документа: <%=request.getAttribute("regNumber")%></li>
    <li>Дата регистрации документа:  <%=request.getAttribute("registrationDate")%></li>
</ol>
<% String prvurl = request.getHeader("Referer");%>
<a  href="<%=prvurl%>">Вернуться назад</a>
</body>
</html>