package ru.documents.factory;

import ru.entity.document.Document;
import ru.exceptions.DocumentExistException;

/**
 * Этот интерфейс реализуют фабрики, которые создают документы разных видов
 *
 * @author BespoyasovaV
 */
public interface DocumentCreate<T extends Document> {
    /**
     * Заполняет поля пустого докуметна, но не все, а те, которые относятся к его классу
     */
    T create() throws DocumentExistException;
}


