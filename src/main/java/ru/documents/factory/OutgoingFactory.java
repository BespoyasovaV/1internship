package ru.documents.factory;

import ru.entity.document.Outgoing;
import ru.enums.Delivery;
import ru.exceptions.DocumentExistException;
import ru.generate.RandomValueService;
import ru.persons.single.PersonService;

/**
 * Класс создает  документ Outgoing, но без полей родителя
 *
 * @author BespoyasovaV
 */
public class OutgoingFactory extends AbstractFactory<Outgoing> implements DocumentCreate<Outgoing> {
    /**
     * {@inheritDoc}
     */
    @Override
    public Outgoing create() throws DocumentExistException {
        Outgoing document = new Outgoing();
        generateRandomProperties(document);
        document.setDestination(RandomValueService.getPersonFromList(PersonService.getPerson()));
        document.setDelivery((Delivery) RandomValueService.generateEnum(Delivery.values()));
        return document;
    }
}
