package ru.documents.factory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.MyProperties;
import ru.entity.document.Document;
import ru.exceptions.DocumentExistException;
import ru.generate.ProgramCollections;
import ru.generate.RandomValueService;
import ru.persons.single.PersonService;

import java.io.IOException;
import java.util.UUID;

/**
 * Заполянет поля документа и возвращает его
 *
 * @author BespoyasovaV
 */
public abstract class AbstractFactory<T> {
    private static final Logger logger = LoggerFactory.getLogger(AbstractFactory.class);
    private static final int generateNumber = Integer.parseInt(MyProperties.getOutOfProperties("generate_count_number"));

    /**
     * С помощью рандома заполняет поля докумета
     */
    void generateRandomProperties(Document document) throws DocumentExistException {

        document.setId(UUID.randomUUID());
        document.setName(RandomValueService.generateListMeaning(ProgramCollections.wordListOne,
                RandomValueService.myRandom.nextInt(ProgramCollections.wordListOne.size())));
        document.setText(RandomValueService.generateListMeaning(ProgramCollections.textList,
                RandomValueService.myRandom.nextInt(ProgramCollections.textList.size())));
        document.setRegNumber(RandomValueService.generateRandomNumber(generateNumber));
        document.setRegistrationDate(RandomValueService.generateDate());
        document.setAuthor(RandomValueService.getPersonFromList(PersonService.getPerson()));
    }
}

