package ru.documents.factory;

import ru.entity.document.Task;
import ru.exceptions.DocumentExistException;
import ru.generate.RandomValueService;
import ru.persons.single.PersonService;

/**
 * Класс создает  документ Task, но без полей родителя
 *
 * @author BespoyasovaV
 */
public class TaskFactory extends AbstractFactory<Task> implements DocumentCreate<Task> {
    /**
     * {@inheritDoc}
     */
    @Override
    public Task create() throws DocumentExistException {
        Task document = new Task();
        generateRandomProperties(document);
        document.setDateIssue(RandomValueService.generateDate());
        document.setTerm(RandomValueService.myRandom.nextInt(RandomValueService.OUTGOING_NUMBER_AFTER));
        document.setExecutorName(RandomValueService.getPersonFromList(PersonService.getPerson()));
        document.setSign(RandomValueService.myRandom.nextBoolean());
        document.setController(RandomValueService.myRandom.nextBoolean());
        return document;
    }
}
