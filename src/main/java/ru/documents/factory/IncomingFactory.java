package ru.documents.factory;


import ru.entity.document.Incoming;
import ru.exceptions.DocumentExistException;
import ru.generate.RandomValueService;
import ru.persons.single.PersonService;

/**
 * Класс создает  документ Incoming, но без полей родителя
 *
 * @author BespoyasovaV
 */
public class IncomingFactory extends AbstractFactory<Incoming> implements DocumentCreate<Incoming> {
    /**
     * {@inheritDoc}
     */
    @Override
    public Incoming create() throws DocumentExistException {
        Incoming document = new Incoming();
        generateRandomProperties(document);
        document.setSender(RandomValueService.getPersonFromList(PersonService.getPerson()));
        document.setDestination(RandomValueService.getPersonFromList(PersonService.getPerson()));
        document.setOutgoingNumber(RandomValueService.myRandom.nextInt(RandomValueService.OUTGOING_NUMBER_AFTER));
        document.setOutgoingDate(RandomValueService.generateDate());
        return document;
    }
}
