package ru.documents.process;
import ru.entity.document.Document;
import ru.generate.PersonDocuments;
import ru.generate.ProgramCollections;
import ru.persons.generate.StaffCollection;
import ru.persons.single.PersonService;

import java.util.ArrayList;
import java.util.List;

/**
 * Класс для обработки(сортировки и вывода документов)
 *
 * @author BespoyasovaV
 */
public class ProcessingDocuments {
    /**
     * Метод заносит документы из listDocument в мапу и сортирует по авторам
     */
    public static void populate() {
        for (int i = 0; i < PersonService.getPerson().size(); i++) {
            List<Document> docList = PersonDocuments.personDocuments.get(StaffCollection.listSurname.get(i));
            if (docList == null) {
                docList = new ArrayList<>();
            }
            for (Document document : ProgramCollections.documents) {
                if (document.getAuthor().getSurname().equals(StaffCollection.listSurname.get(i))) {
                    docList.add(document);
                    PersonDocuments.personDocuments.put(StaffCollection.listSurname.get(i) + " "
                            + StaffCollection.listName.get(i) + " " + StaffCollection.listPatronymic.get(i), docList);
                }
            }
        }
    }
}