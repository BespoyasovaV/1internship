package ru.generate;

import ru.documents.factory.DocumentCreate;
import ru.documents.factory.IncomingFactory;
import ru.documents.factory.OutgoingFactory;
import ru.documents.factory.TaskFactory;
import ru.enums.TypeOfDocument;

/**
 * Клас для создания рандомной фабрики
 *
 * @author BespoyasovaV
 */
public class GenerateRandomFactory {
    /**
     * Метод создает рандомную фабрику
     */
    public static DocumentCreate generateRandomFactory() {
        DocumentCreate documentCreate = null;
        switch ((TypeOfDocument) RandomValueService.generateEnum(TypeOfDocument.values())) {
            case TASK:
                documentCreate = new TaskFactory();
                break;

            case INCOMING:
                documentCreate = new IncomingFactory();
                break;

            case OUTGOING:
                documentCreate = new OutgoingFactory();
                break;
        }
        return documentCreate;
    }
}
