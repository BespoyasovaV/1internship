package ru.generate;


import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.entity.staff.Person;
import ru.exceptions.DocumentExistException;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Random;
import java.util.GregorianCalendar;
import java.util.List;


/**
 * Класс для генерации  рандомных значений
 *
 * @author BespoyasovaV
 */
public final class RandomValueService {
    private RandomValueService(){}
    private static final Logger logger = LoggerFactory.getLogger(RandomValueService.class);
    public static Random myRandom = new Random();
    public static final int OUTGOING_NUMBER_AFTER = 35;

    /**
     * Метод генерирует регистрационный номер
     */
    public static int generateRandomNumber(int beforeNumber) throws DocumentExistException {
        int randNumber = 1 + myRandom.nextInt(beforeNumber);
        if (ProgramCollections.regNumCollection.contains(randNumber)) {
          throw new DocumentExistException("Документ с генерируемым номером уже существует");
        } else {
            ProgramCollections.regNumCollection.add(randNumber);
        }
        return randNumber;
    }

    /**
     * Метод возвращает рандомный элемент переданной коллекции
     */
    public static <T> T generateListMeaning(Collection<T> list, int index) {
        int randomValue = myRandom.nextInt(list.size());
        return CollectionUtils.get(list,index);
    }

    /**
     * Метод возвращает рандомный элемент из enum Delivery
     */
    public static Enum generateEnum(Enum[] myEnum) {
        int randomEnum = myRandom.nextInt(myEnum.length);
        return myEnum[randomEnum];
    }

    /**
     * Метод генерирует дату
     */
    public static Date generateDate() {
        int yearRand = 2000 + myRandom.nextInt(25);
        int monthRand = 1 + myRandom.nextInt(10);
        int dayRand = 1 + myRandom.nextInt(29);
        Calendar calendar = new GregorianCalendar(yearRand, monthRand, dayRand);
        Date date = calendar.getTime();
        return date;
    }

    /**
     * Метод выбирает рандомное значение из списка Person-ов, взятых их xml
     */
    public static Person getPersonFromList(List<Person> list) {
        int randomElementFromList = myRandom.nextInt(list.size());
        return list.get(randomElementFromList);
    }
}