package ru.read;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.entity.staff.Department;
import ru.entity.staff.Organization;
import ru.entity.staff.Person;
import ru.entity.staff.Staff;
import ru.entity.wrapper.Wrapper;
import ru.persons.single.DepartmentService;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import java.io.InputStream;
import java.util.List;

/**
 * Класс для чтения .xml
 *
 * @author BespoyasovaV
 */
public final class Unmarshall {
    private static final Logger logger = LoggerFactory.getLogger(DepartmentService.class);
    static JAXBContext jc;
    private static Unmarshaller unmarshaller;

    private Unmarshall() {
    }

    static {
        try {
            jc = JAXBContext.newInstance(Wrapper.class, Person.class, Organization.class, Department.class, Staff.class);
            unmarshaller = jc.createUnmarshaller();
        } catch (JAXBException e) {
            logger.error("Вызвано исключение в Unmarshall");
        }
    }

    /**
     * Метод принимает 3 параметра: Unmarshaller, имя класса и путь до файла, который нужно читать. Возвращает лист с
     * прочитанным
     */
    public static <T extends Staff> List<T> unmarshal(InputStream filePath) throws JAXBException {
        StreamSource xml = new StreamSource(filePath);
        Wrapper<T> wrapper = (Wrapper<T>) unmarshaller.unmarshal(xml, Wrapper.class).getValue();
        return wrapper.getItems();
    }
}


