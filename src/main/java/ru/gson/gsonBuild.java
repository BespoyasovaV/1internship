package ru.gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import ru.entity.document.Document;
import ru.generate.PersonDocuments;

import java.io.FileWriter;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.List;

/**
 * Класс для записи документов аторов в *.json
 *
 * @author BespoyasovaV
 */
public final class gsonBuild {
    static GsonBuilder builder = new GsonBuilder();
    static Gson gson = builder.setPrettyPrinting().create();

    private gsonBuild() {
    }

    /**
     * Метод создает название файла
     */
    public static String messageFile(List<Document> map) {
        return MessageFormat.format("{0}.json", map.get(0).getAuthor().getSurname());

    }

    public static void writeJson() throws IOException {
        for (String key : PersonDocuments.personDocuments.keySet()) {
            FileWriter writer = new FileWriter(messageFile(PersonDocuments.personDocuments.get(key)));
            writer.write(gson.toJson((PersonDocuments.personDocuments.get(key))));
            writer.close();
        }
    }
}
