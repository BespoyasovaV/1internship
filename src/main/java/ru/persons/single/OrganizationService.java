package ru.persons.single;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.entity.staff.Organization;
import ru.h2DB.ConnectionDatabase;
import ru.h2DB.OrganizationDatabase;
import ru.read.Unmarshall;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.List;

/**
 * Класс-одиночка Person
 *
 * @author BespoyasovaV
 */
public final class OrganizationService extends ConnectionDatabase {
    private static final Logger logger = LoggerFactory.getLogger(OrganizationService.class);
    private static List<Organization> organizationList = null;
    private static final String FILE_PATH = "organization.xml";
    private static final String CREATE = "CREATE TABLE ORGANIZATION(id varchar , phone varchar, fullName varchar (250) , shortName varchar(255),name varchar(60),surname varchar (255), patronymic varchar (255))";
    private static OrganizationService organizationSingle;


    @Override
    public int hashCode() {
        return super.hashCode();
    }

    private OrganizationService() {
    }

    public static OrganizationService getOrganizationSingle() {
        if (organizationSingle == null) {
            synchronized (OrganizationService.class) {
                organizationSingle = new OrganizationService();
            }
        }
        return organizationSingle;
    }

    /**
     * Метод читает .xml и возращае прочитанное в листе
     */
    public static List getOrganization() {
        try (InputStream inputStream = OrganizationService.class.getClassLoader().getResourceAsStream(FILE_PATH)) {
            organizationList = Unmarshall.unmarshal(inputStream);
        } catch (JAXBException e) {
            logger.error("error in OrganizationService (JAXBException)");
        } catch (IOException e) {
            logger.error("error in OrganizationService (IOException)");
        }
        return organizationList;
    }
}

