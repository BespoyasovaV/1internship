package ru.persons.single;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.entity.staff.Person;
import ru.h2DB.ConnectionDatabase;
import ru.h2DB.PersonDatabase;
import ru.read.Unmarshall;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.List;

/**
 * Класс-одиночка Person
 *
 * @author BespoyasovaV
 */
public final class PersonService extends ConnectionDatabase {
    private static final Logger logger = LoggerFactory.getLogger(PersonService.class);
    private static List<Person> personList;
    private final static String FILE_PATH = "person.xml";
    private static volatile PersonService personSingle;

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    private PersonService() {
    }

    public static PersonService getPersonSingle() {
        if (personSingle == null) {
            synchronized (PersonService.class) {
                if (personSingle == null) {
                    personSingle = new PersonService();
                }
            }
        }
        return personSingle;
    }

    /**
     * Метод читает .xml и возращае прочитанное в листе
     */
    public static List<Person> getPerson() {
        try (InputStream inputStream = PersonService.class.getClassLoader().getResourceAsStream(FILE_PATH)) {
            personList = Unmarshall.unmarshal(inputStream);
        } catch (JAXBException e) {
            logger.error("error in PersonService (JAXBException)");
        } catch (IOException e) {
            logger.error("error in PersonService (IOException)");
        }
        return personList;
    }
}
