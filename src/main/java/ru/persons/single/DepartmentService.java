package ru.persons.single;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.entity.staff.Department;
import ru.h2DB.ConnectionDatabase;
import ru.h2DB.DepartmentDatabase;
import ru.read.Unmarshall;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.List;

/**
 * Класс-одиночка Department
 *
 * @author BespoyasovaV
 */
public final class DepartmentService extends ConnectionDatabase {
    private static final Logger logger = LoggerFactory.getLogger(DepartmentService.class);
    private static List<Department> departmentList = null;
    private static final String FILE_PATH = "department.xml";
    private static DepartmentService departmentSingle;

    private DepartmentService() {
    }

    /**
     * Метод проверяет, есть ли объект DepartmentService, если нет- создает
     */
    public static DepartmentService getDepartmentSingle() {
        if (departmentSingle == null) {
            synchronized (DepartmentService.class) {
                departmentSingle = new DepartmentService();
            }
        }
        return departmentSingle;
    }

    /**
     * Метод читает .xml и возращае прочитанное в листе
     */
    public static List getDepartment() {
        try (InputStream inputStream = DepartmentService.class.getClassLoader().getResourceAsStream(FILE_PATH)) {
            departmentList = Unmarshall.unmarshal(inputStream);
        } catch (JAXBException e) {
            logger.error("error in DepartmentService (JAXBException)");
        } catch (IOException e) {
            logger.error("error in DepartmentService (IOException)");
        }
        return departmentList;
    }
}

