package ru.persons.generate;

import java.util.Arrays;
import java.util.List;

public final class StaffCollection {
    private StaffCollection() {
    }
    public static List<String> listName;
    public static List<String> listSurname;
    public static List<String> listPatronymic;
    public static List<String> listPost;
    public static List<String> listPhone;

    static {
        listName = Arrays.asList("Сергей", "Анфиса", "Надежда", "Юлия", "Кристина");
        listSurname = Arrays.asList("Барсуков", "Березнянкова", "Киреева", "Никитина", "Полянская");
        listPatronymic = Arrays.asList("Геннадиевич", "Кирилловна", "Семеновна", "Игоревна", "Викторовна");
        listPost = Arrays.asList("менеджер", "директор", "стажер");
        listPhone = Arrays.asList("8(937)-493-32-96", "8(918)-266-13-91", "8(917)-739-39-14");
    }

}
