package ru.enums;

/**
 * Варианты доставки
 *
 * @author BespoyasovaV
 */
public enum Delivery {
    PICK_UP_SERVICE,
    COURIER,
    RUSSIAN_POST
}