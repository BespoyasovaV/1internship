package ru.enums;
/**
 * Виды документов
 *
 * @author BespoyasovaV
 */
public enum TypeOfDocument {
    TASK,
    INCOMING,
    OUTGOING
}
