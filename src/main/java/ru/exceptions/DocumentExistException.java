package ru.exceptions;

/**
 * Исключение для генерируемого номера
 *
 * @author BespoyasovaV
 */
public class DocumentExistException extends Exception {
    public DocumentExistException(String description) {
        super(description);
    }
}
