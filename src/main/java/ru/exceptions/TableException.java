package ru.exceptions;

/**
 * Исключение для непраильно соданных таблиц
 *
 * @author BespoyasovaV
 */
public class TableException extends Exception  {
    public TableException(String message) throws  Exception {
        super(message);
    }
}
