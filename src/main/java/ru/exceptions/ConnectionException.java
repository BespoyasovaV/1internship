package ru.exceptions;
/**
 *Исключение для Connection, если не удастся связаться с БД
 */
public class ConnectionException extends Exception {
    public ConnectionException(String message) throws  Exception {
        super(message);
    }
}
