package ru.h2DB;

import ru.entity.staff.Staff;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

/**
 * @author BespoyasovaV
 * Этот интерфейс реализуют классы с БД
 */
public interface CRUD<T extends Staff> {
    /**
     * Метод заполняет таблицу
     */
    void create(T t) throws Exception;

    /**
     * Метод читает таблицу
     */
  List<String> read() throws Exception;

    /**
     * Метод обновляет таблицу
     */
    void update() throws Exception;

    /**
     * Метод удаляет запись из таблицы
     */
    void delete(UUID id) throws Exception;

    /**
     * Метод возвращает запись по Id
     */
    T findById(UUID t) throws Exception;
}
