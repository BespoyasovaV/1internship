package ru.h2DB;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.entity.staff.Department;
import ru.h2DB.ObjectToString.DepartmentObjectToString;
import ru.h2DB.RowMapper.DepartmentRowMapper;

import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Класс-одиночка для записи информации об отделениях в БД
 *
 * @author BespoyasovaV
 */
public class DepartmentDatabase extends ConnectionDatabase implements CRUD<Department> {
    private static final String INSERT = "INSERT INTO DEPARTMENT(id,phone,fullName,shortName,name,surname, patronymic) VALUES(?,?,?,?,?,?,?)";
    private static final String SELECT = "SELECT * FROM DEPARTMENT";
    private static final String SELECT_BY_ID = "SELECT * FROM DEPARTMENT WHERE id=?";
    private static final String DELETE = "DELETE FROM DEPARTMENT WHERE id = ?";
    private static DepartmentDatabase h2dbDepartment;
    private static final Logger logger = LoggerFactory.getLogger(DepartmentDatabase.class);
    DepartmentRowMapper departmentRowMapper=new DepartmentRowMapper();
    DepartmentObjectToString departmentObjectToString=new DepartmentObjectToString();

    /**
     * Метод для создания одиночки
     */
    public static DepartmentDatabase getH2dbDepartment() {
        if (h2dbDepartment == null) {
            h2dbDepartment = new DepartmentDatabase();
        }
        return h2dbDepartment;
    }

    /**
     * {@inheritDoc}
     */
    public void create(Department department1) throws Exception {
        try (Connection connection = getDBConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT)) {
            preparedStatement.setString(1, department1.getId().toString());
            preparedStatement.setString(2, department1.getPhone());
            preparedStatement.setString(3, department1.getFullName());
            preparedStatement.setString(4, department1.getShortName());
            preparedStatement.setString(5, department1.getLeader().getName());
            preparedStatement.setString(6, department1.getLeader().getSurname());
            preparedStatement.setString(7, department1.getLeader().getPatronymic());
            preparedStatement.executeUpdate();
        }
    }

    /**
     * {@inheritDoc}
     */
    public List<String> read() throws Exception {
        List<String> departmentList = new ArrayList<String>();
        try (Connection connection = getDBConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(SELECT)) {
            while (resultSet.next()) {
                departmentList.add(departmentObjectToString.toString(departmentRowMapper.rowMapper(resultSet)));
            }
        } catch (SQLException e) {
            logger.error("SQLException in DepartmentDatabase");
        }
        return departmentList;
    }

    /**
     * {@inheritDoc}
     */
    public void update() throws Exception {
        try (Connection connection = getDBConnection();
             PreparedStatement preparedStatement = (PreparedStatement) connection.createStatement()) {
            preparedStatement.executeUpdate();
        }
    }

    /**
     * {@inheritDoc}
     */
    public void delete(UUID department) throws Exception {
        try (Connection connection = getDBConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
            preparedStatement.setString(1, department.toString());
            preparedStatement.executeUpdate();
        }
    }

    /**
     * {@inheritDoc}
     */
    public Department findById(UUID department1) throws Exception {
        Department department = null;
        try (Connection connection = getDBConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_ID)) {
            preparedStatement.setString(1, department1.toString());
            ResultSet resultSet = preparedStatement.executeQuery(SELECT_BY_ID);
            while (resultSet.next()) {
                department = departmentRowMapper.rowMapper(resultSet);
            }
        }
        return department;
    }
}

