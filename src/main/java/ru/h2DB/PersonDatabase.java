package ru.h2DB;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.entity.staff.Person;
import ru.h2DB.ObjectToString.PersonObjectToString;
import ru.h2DB.RowMapper.PersonRowMapper;

import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Класс-одиночка для записи информации об Сотрудниках в БД
 *
 * @author BespoyasovaV
 */
public class PersonDatabase extends ConnectionDatabase implements CRUD<Person> {
    private static final String INSERT = "INSERT INTO PERSON(id,name, surname, patronymic,post,birthDay,PHONENUMBER) VALUES(?,?,?,?,?,?,?)";
    private static final String SELECT = "SELECT * FROM PERSON";
    private static final String SELECT_BY_ID = "SELECT * FROM  PERSON WHERE id=?";
    private static final String DELETE = "DELETE FROM PERSON WHERE id = ?";
    private static PersonDatabase h2dbPerson;
    PersonRowMapper personRowMapper=new PersonRowMapper();
    PersonObjectToString personObjectToString=new PersonObjectToString();
    private static final Logger logger = LoggerFactory.getLogger(PersonDatabase.class);

    /**
     * Метод получения одиночки
     */
    public static PersonDatabase getH2dbPerson() {
        if (h2dbPerson == null) {
            h2dbPerson = new PersonDatabase();
        }
        return h2dbPerson;
    }

    /**
     * {@inheritDoc}
     */
    public void create(Person person1) throws Exception {
        try (Connection connection = getDBConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT)) {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            String testDateString = df.format(person1.getBirthDay());
            preparedStatement.setString(1, person1.getId().toString());
            preparedStatement.setString(2, person1.getName());
            preparedStatement.setString(3, person1.getSurname());
            preparedStatement.setString(4, person1.getPatronymic());
            preparedStatement.setString(5, person1.getPost());
            preparedStatement.setString(6, testDateString);
            preparedStatement.setString(7, person1.getPhoneNumber());
            preparedStatement.executeUpdate();
        }
    }

    /**
     * {@inheritDoc}
     */
    public List<String> read() throws Exception {
        List<String> personJson = new ArrayList<>();
        try (Connection connection = getDBConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(SELECT)) {
            while (resultSet.next()) {
                personJson.add(personObjectToString.toString(personRowMapper.rowMapper(resultSet)));
            }
        } catch (SQLException e) {
            logger.error("SQLException in PersonDatabase");
        }
        return personJson;
    }

    /**
     Метод взвращеет лист с Person
     */
    public List<Person> readb() throws Exception {
        List<Person> personArrayList = new ArrayList<>();
        try (Connection connection = getDBConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(SELECT)) {
            while (resultSet.next()) {
                Person p=new Person();
               p=personRowMapper.rowMapper(resultSet);
                personArrayList.add(p);
            }
        } catch (SQLException e) {
            logger.error("SQLException in PersonDatabase");
        }
        return personArrayList;
    }

    /**
     * {@inheritDoc}
     */
    public void update() throws Exception {
        try (Connection connection = getDBConnection();
             PreparedStatement preparedStatement = (PreparedStatement) connection.createStatement();) {
            preparedStatement.executeUpdate();
        }
    }

    /**
     * {@inheritDoc}
     */
    public void delete(UUID person) throws Exception {
        try (Connection connection = getDBConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
            preparedStatement.setString(1, person.toString());
        }
    }

    /**
     * {@inheritDoc}
     */
    public Person findById(UUID person) throws Exception {
       Person person1=null;
        try (Connection connection = getDBConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_ID)) {
            preparedStatement.setString(1, person.toString());
            ResultSet resultSet = preparedStatement.executeQuery(SELECT_BY_ID);
            while (resultSet.next()) {
                person1 = personRowMapper.rowMapper(resultSet);
            }
        }
        return person1;
    }
}
