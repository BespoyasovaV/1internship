package ru.h2DB;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.entity.staff.Organization;
import ru.h2DB.ObjectToString.OrganizationObjectToString;
import ru.h2DB.RowMapper.OrganizationRowMapper;

import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


/**
 * Класс -одиночкадля записи информации об организациях в БД
 *
 * @author BespoyasovaV
 */
public class OrganizationDatabase extends ConnectionDatabase  implements CRUD<Organization> {
    private static final String INSERT = "INSERT INTO ORGANIZATION(id,phone,fullName,shortName,name,surname, patronymic) VALUES(?,?,?,?,?,?,?)";
    private static final String SELECT = "SELECT * FROM ORGANIZATION";
    private static final String SELECT_BY_ID = "SELECT * FROM ORGANIZATION WHERE id=?";
    private static final String DELETE = "DELETE FROM ORGANIZATION WHERE id = ?";
    private static OrganizationDatabase h2dbOrganization;
    private static final Logger logger = LoggerFactory.getLogger(OrganizationDatabase.class);
    OrganizationRowMapper organizationRowMapper=new OrganizationRowMapper();
    OrganizationObjectToString organizationObjectToString=new OrganizationObjectToString();

    /**
     * Метод для получения одиночки
     */
    public static OrganizationDatabase getH2dbOrganization() {
        if (h2dbOrganization == null) {
            h2dbOrganization = new OrganizationDatabase();
        }
        return h2dbOrganization;
    }

    /**
     * {@inheritDoc}
     */
    public void create(Organization organization1) throws Exception {
        try (Connection connection = getDBConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT)) {
            preparedStatement.setString(1, organization1.getId().toString());
            preparedStatement.setString(2, organization1.getPhone());
            preparedStatement.setString(3, organization1.getFullName());
            preparedStatement.setString(4, organization1.getShortName());
            preparedStatement.setString(5, organization1.getLeader().getName());
            preparedStatement.setString(6, organization1.getLeader().getSurname());
            preparedStatement.setString(7, organization1.getLeader().getPatronymic());
            preparedStatement.executeUpdate();
        }
    }

    /**
     * {@inheritDoc}
     */
    public List<String> read() {
        List<String> organizationList = new ArrayList<String>();
        try (Connection connection = getDBConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(SELECT)) {
            while (resultSet.next()) {
                organizationList.add(organizationObjectToString.toString(organizationRowMapper.rowMapper(resultSet)));
            }
        } catch (Exception e) {
            logger.error("(SQLException in OrganizationDatabase");
        }
        return organizationList;
    }

    /**
     * {@inheritDoc}
     */
    public void update() throws Exception {
        try (Connection connection = getDBConnection();
             PreparedStatement preparedStatement = (PreparedStatement) connection.createStatement()) {
            preparedStatement.executeUpdate();
        }
    }

    /**
     * {@inheritDoc}
     */
    public void delete(UUID organization) throws Exception {
        try (Connection connection = getDBConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
            preparedStatement.setString(1, organization.toString());
            preparedStatement.executeUpdate();
        }
    }

    /**
     * {@inheritDoc}
     */
    public Organization findById(UUID organization) throws Exception {
        Organization organization1=null;
        try (Connection connection = getDBConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_ID)) {
            preparedStatement.setString(1, organization.toString());
            ResultSet resultSet = preparedStatement.executeQuery(SELECT_BY_ID);
            while (resultSet.next()) {
                organization1 =organizationRowMapper.rowMapper(resultSet);
            }
        }
        return organization1;
    }
}
