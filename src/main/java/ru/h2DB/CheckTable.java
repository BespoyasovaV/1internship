package ru.h2DB;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;

/**
 * Класс  для проверки корректности таблиц
 *
 * @author BespoyasovaV
 */
public class CheckTable extends ConnectionDatabase {
    /**
     * Метод проверяет, существует ли таблица
     */
    public static Boolean checkForExistence(String tableName) throws Exception {
        try (Connection connection = getDBConnection()) {
            DatabaseMetaData md = null;
            md = connection.getMetaData();
            ResultSet rs = null;
            rs = md.getTables(null, null, tableName, null);
            if (rs.next()) {
                return true;
            } else {
                return false;
            }
        }
    }
}
