package ru.h2DB;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.MyProperties;
import ru.exceptions.ConnectionException;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.DriverManager;
import java.text.MessageFormat;


/**
 * @author BespoyasovaV
 * Класс для получения связи с БД
 */
public class ConnectionDatabase {
    private static final Logger logger = LoggerFactory.getLogger(ConnectionDatabase.class);
    private static final String DB_DRIVER = MyProperties.getOutOfProperties("driver");
    private static final String DB_CONNECTION = MyProperties.getOutOfProperties("connection");
    private static final String DB_USER = "";
    private static final String DB_PASSWORD = "";

    /**
     * Метод для создания таблицы
     */
    public static void createTable(String name, String create)  {
        try (Connection connection = DepartmentDatabase.getDBConnection()) {
            DatabaseMetaData dbm = connection.getMetaData();
            ResultSet table = dbm.getTables(null, null, name, null);
            if (table.next()) {
                logger.info( MessageFormat.format("Таблица {0} создана",name));
                return;
            } else {
                Statement statement = connection.createStatement();
                statement.execute(create);
            }
        } catch (Exception e) {
            logger.error("Не удалось связаться с БД");
        }
    }

    /**
     * Метод для получения связи с БД
     */
    public static Connection getDBConnection() throws Exception {
        Connection dbConnection = null;
        try {
            Class.forName(DB_DRIVER);
        } catch (ClassNotFoundException e) {
            logger.error(String.valueOf(e.getException()));
        }
        try {
            dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER,
                    DB_PASSWORD);
            return dbConnection;
        } catch (SQLException e) {
            logger.error(String.valueOf(e.getNextException()));
        }
        if (dbConnection != null) {
            return dbConnection;
        } else {
            throw new ConnectionException("Не удалось связаться с БД");
        }
    }
    }


