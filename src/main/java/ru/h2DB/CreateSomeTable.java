package ru.h2DB;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.entity.staff.Department;
import ru.entity.staff.Organization;
import ru.entity.staff.Person;
import ru.persons.single.DepartmentService;
import ru.persons.single.OrganizationService;
import ru.persons.single.PersonService;

import java.sql.SQLException;
import java.util.List;

import static ru.h2DB.ConnectionDatabase.createTable;

/**
 * Класс для создания таблиц
 *
 * @author BespoyasovaV
 */
public class CreateSomeTable {
    public static final String CREATE_PERSON_TABLE = "CREATE TABLE PERSON(id varchar (250) , name varchar(255),surname varchar (255), patronymic varchar (255)," +
            "post varchar (255),birthDay varchar , phoneNumber varchar (15))";
    public static final String CREATE_DEPARTMENT_TABLE = "CREATE TABLE DEPARTMENT(id varchar , phone varchar, fullName varchar (250) , shortName varchar(255),name varchar(60),surname varchar (255), patronymic varchar (255))";
    public static final String CREATE_ORGANIZATION_TABLE = "CREATE TABLE ORGANIZATION(id varchar , phone varchar, fullName varchar (250) , shortName varchar(255),name varchar(60),surname varchar (255), patronymic varchar (255))";
    private static final Logger logger = LoggerFactory.getLogger(CreateSomeTable.class);

    /**
     * Метод создает нужную таблицу и заполняет ее
     */
    public static void createOneOfTheTable(String str, String createString) throws SQLException {
        createTable(str, createString);
        switch (str) {
            case ("Person"):
                List<Person> listPerson = PersonService.getPerson();
                PersonDatabase h2dbPerson = PersonDatabase.getH2dbPerson();
                for (Person listPerson1 : listPerson) {
                    try {
                        h2dbPerson.create(listPerson1);
                    } catch (Exception e) {
                        logger.error("Не удалось связаться с БД");
                    }
                }
                break;
            case ("Department"):
                List<Department> listDepartment = DepartmentService.getDepartment();
                DepartmentDatabase h2dbDepartment = DepartmentDatabase.getH2dbDepartment();
                for (Department department : listDepartment) {
                    try {
                        h2dbDepartment.create(department);
                    } catch (Exception e) {
                        logger.error("Не удалось связаться с БД");
                    }
                }
                break;
            case ("Organization"):
                List<Organization> listOrganization = OrganizationService.getOrganization();
                OrganizationDatabase h2dbOrganization = OrganizationDatabase.getH2dbOrganization();
                for (Organization organization : listOrganization) {
                    try {
                        h2dbOrganization.create(organization);
                    } catch (Exception e) {
                        logger.error("Не удалось связаться с БД");
                    }
                }
                break;
        }
    }
}

