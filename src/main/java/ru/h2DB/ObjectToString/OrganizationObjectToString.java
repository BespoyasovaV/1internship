package ru.h2DB.ObjectToString;

import ru.entity.staff.Organization;

/**
 * Класс поможет переводить Object -> String
 *
 * @author BespoyasovaV
 */
public class OrganizationObjectToString implements ObjectToString<Organization> {
    private static OrganizationObjectToString organizationObjectToString;

    /**
     * Метод проверяет, есть ли объект DepartmentObjectToString, если нет- создает
     */
    public static OrganizationObjectToString getOrganizationObjectToString() {
        if (organizationObjectToString == null) {
            organizationObjectToString = new OrganizationObjectToString();
        }
        return organizationObjectToString;
    }

    /**
     * {@inheritDoc}
     */
    public String toString(Organization organization) {
        String str = "id=" + organization.getId() + " " + "phone=" + organization.getPhone() + "fullName=" + organization.getFullName() + " " + "shortName=" + organization.getShortName() + " " + "name=" + organization.getLeader().getName() + " " + "surname=" + organization.getLeader().getSurname() + " " + "patronymic=" + organization.getLeader().getPatronymic();
        return str;
    }
}
