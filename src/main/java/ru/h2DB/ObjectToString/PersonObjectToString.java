package ru.h2DB.ObjectToString;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.entity.staff.Person;

/**
 *Класс поможет переводить Object -> String
 * @author BespoyasovaV
 */
public class PersonObjectToString implements ObjectToString<Person>{
    private static final Logger logger = LoggerFactory.getLogger(PersonObjectToString.class);
    private static PersonObjectToString personObjectToString;

    /**
     * Метод проверяет, есть ли объект DepartmentObjectToString, если нет- создает
     */
    public static PersonObjectToString getPersonObjectToString() {
        if (personObjectToString == null) {
           personObjectToString=new PersonObjectToString();
        }
        return personObjectToString;
    }
    /**
     * {@inheritDoc}
     */
    public String toString(Person person) {
        ObjectMapper mapper = new ObjectMapper();
        String jsonString = null;
        try {
            jsonString = mapper.writeValueAsString(person);
        } catch (JsonProcessingException e) {
            logger.error("JsonProcessingException in PersonRowMapper");
        }
        return jsonString;
    }
}
