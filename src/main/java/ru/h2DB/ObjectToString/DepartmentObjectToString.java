package ru.h2DB.ObjectToString;

import ru.entity.staff.Department;

/**
 *Класс поможет переводить Object -> String
 * @author BespoyasovaV
 */
public class DepartmentObjectToString implements ObjectToString<Department>{
    private static DepartmentObjectToString departmentObjectToString;

    /**
     * Метод проверяет, есть ли объект DepartmentObjectToString, если нет- создает
     */
    public static DepartmentObjectToString getDepartmentObjectToString() {
        if (departmentObjectToString == null) {
            departmentObjectToString = new DepartmentObjectToString();
        }
        return departmentObjectToString;
    }
        /**
         * {@inheritDoc}
         */
        public String toString(Department department) {
            String str = "id=" + department.getId() + " " + "phone=" + department.getPhone() + "fullName=" + department.getFullName() + " " + "shortName=" + department.getShortName() + " " + "name=" + department.getLeader().getName() + " " + "surname=" + department.getLeader().getSurname() + " " + "patronymic=" + department.getLeader().getPatronymic();
            return str;
        }
        }
