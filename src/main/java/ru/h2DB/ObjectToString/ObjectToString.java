package ru.h2DB.ObjectToString;

import ru.entity.staff.Staff;
/**
 * Интерфейс поможет переводить Object -> String
 *
 * @author BespoyasovaV
 */
public interface ObjectToString <T extends Staff> {
    /**
     *Метод переводит объект в строку
     */
    String toString(T t);
}
