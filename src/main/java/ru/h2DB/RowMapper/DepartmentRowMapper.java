package ru.h2DB.RowMapper;

import ru.entity.staff.Department;
import ru.entity.staff.Person;
import ru.h2DB.ObjectToString.ObjectToString;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

/**
 * Класс помогает читать данные и таблицы
 *
 * @author BespoyasovaV
 */
public class DepartmentRowMapper implements Mapper<Department> {
    private static DepartmentRowMapper departmentRowMapper;

    /**
     * Метод проверяет, есть ли объект DepartmentRowMapper, если нет- создает
     */
    public static DepartmentRowMapper getDepartmentRowMapper() {
        if (departmentRowMapper == null) {
            departmentRowMapper = new DepartmentRowMapper();
        }
        return departmentRowMapper;
    }

    /**
     * {@inheritDoc}
     */
    public Department rowMapper(ResultSet resultSet) throws SQLException {
        Department department = null;
        Person person = null;
        department.setId(UUID.fromString(resultSet.getString(1)));
        department.setPhone(resultSet.getNString(2));
        department.setFullName(resultSet.getString(3));
        department.setShortName(resultSet.getString(4));
        person.setName(resultSet.getString(5));
        person.setSurname(resultSet.getString(6));
        person.setPatronymic(resultSet.getString(7));
        department.setLeader(person);
        return department;
    }

}


