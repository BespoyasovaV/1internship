package ru.h2DB.RowMapper;

import ru.entity.staff.Organization;
import ru.entity.staff.Person;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;
/**
 * Класс помогает читать данные и таблицы
 *
 * @author BespoyasovaV
 */
public class OrganizationRowMapper implements Mapper<Organization> {
    private static OrganizationRowMapper organizationRowMapper;
    /**
     * Метод проверяет, есть ли объект OrganizationRowMapper, если нет- создает
     */
    public static OrganizationRowMapper getOrganizationRowMapper() {
        if (organizationRowMapper == null) {
            organizationRowMapper = new OrganizationRowMapper();
        }
        return organizationRowMapper;
    }
    /**
     * {@inheritDoc}
     */
    public Organization rowMapper(ResultSet resultSet) throws SQLException {
        Organization organization = null;
        Person person = null;
        organization.setId(UUID.fromString(resultSet.getString(1)));
        organization.setPhone(resultSet.getNString(2));
        organization.setFullName(resultSet.getString(3));
        organization.setShortName(resultSet.getString(4));
        person.setName(resultSet.getString(5));
        person.setSurname(resultSet.getString(6));
        person.setPatronymic(resultSet.getString(7));
        organization.setLeader(person);
        return organization;
    }
}
