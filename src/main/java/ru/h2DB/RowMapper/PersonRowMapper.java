package ru.h2DB.RowMapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.entity.staff.Person;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class PersonRowMapper implements Mapper<Person> {
    private static final Logger logger = LoggerFactory.getLogger(PersonRowMapper.class);
    private static PersonRowMapper personRowMapper;

    public static PersonRowMapper getPersonRowMapper() {
        if (personRowMapper == null) {
            personRowMapper = new PersonRowMapper();
        }
        return personRowMapper;
    }
    /**
     * {@inheritDoc}
     */
    public Person rowMapper(ResultSet resultSet) throws SQLException {
        Person person = new Person();
        person.setId(UUID.fromString(resultSet.getString(1)));
        person.setName(resultSet.getString(2));
        person.setSurname(resultSet.getString(3));
        person.setPatronymic(resultSet.getString(4));
        person.setPost(resultSet.getNString(5));
        Date date = null;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd").parse(resultSet.getString(6));
        } catch (ParseException e) {
            logger.error("ParseException in PersonRowMapper");
        }
        person.setBirthDay(date);
        person.setPhoneNumber(resultSet.getString(7));
        return  person;
    }
}

