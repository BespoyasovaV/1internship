package ru.h2DB.RowMapper;

import ru.entity.staff.Staff;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Интерфейс поможет читать данные из БД
 *
 * @author BespoyasovaV
 */
public interface Mapper<T extends Staff> {
    /**
     *Метод читает строки из таблиц
     */
    T rowMapper(ResultSet resultSet) throws SQLException;
}
