package ru.rest;

import ru.entity.staff.Person;
import ru.persons.single.PersonService;

import java.text.MessageFormat;

/**
 * Класс  для перевода Person->String
 *
 * @author BespoyasovaV
 */
public class PersonNameUtils {
    /**
     * Метод переводит ФИО в стринг
     */
    public static String nameUtils(int i) {
        String str = MessageFormat.format("{0} {1} {2}", PersonService.getPerson().get(i).getSurname(), PersonService.getPerson().get(i).getName(), PersonService.getPerson().get(i).getPatronymic());
        return str;
    }

    /**
     * Метод возвращает ФИО person-а в String
     */
    public static String personToString(Person person) {
        String str = MessageFormat.format("{0} {1} {2}", person.getSurname(), person.getName(), person.getPatronymic());
        return str;
    }
}