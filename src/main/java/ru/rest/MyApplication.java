package ru.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.documents.factory.DocumentCreate;
import ru.documents.process.ProcessingDocuments;
import ru.entity.document.Document;
import ru.exceptions.DocumentExistException;
import ru.exceptions.TableException;
import ru.generate.GenerateRandomFactory;
import ru.generate.ProgramCollections;
import ru.generate.RandomValueService;
import ru.h2DB.CheckTable;
import ru.h2DB.CreateSomeTable;


import javax.annotation.PostConstruct;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

/**
 * Класс для запуска приложения
 *
 * @author BespoasovaV
 */
@ApplicationPath("/rest")
public class MyApplication extends Application {
    private static final int NUMBER_OF_DOCUMENTS = 100;
    private static final Logger logger = LoggerFactory.getLogger(MyApplication.class);

    @PostConstruct
    public void makeDocuments() {
        int randomValueForDocList = 0;
        try {
            randomValueForDocList = RandomValueService.generateRandomNumber(NUMBER_OF_DOCUMENTS);
        } catch (DocumentExistException e) {
            logger.error("Документ с генерируемым номером уже существует");
        }
        for (int i = 0; i < randomValueForDocList; i++) {
            DocumentCreate documentCreate = GenerateRandomFactory.generateRandomFactory();
            Document document = null;
            try {
                document = documentCreate.create();
            } catch (DocumentExistException e) {
                logger.error("Документ с генерируемым номером уже существует");
            }
            ProgramCollections.documents.add(document);
        }

        ProcessingDocuments.populate();
        try {
            CreateSomeTable.createOneOfTheTable("Person", CreateSomeTable.CREATE_PERSON_TABLE);
            if (CheckTable.checkForExistence("PERSON")) {
            } else {
                throw new TableException("Таблица Person не создана");
            }

            CreateSomeTable.createOneOfTheTable("Department", CreateSomeTable.CREATE_DEPARTMENT_TABLE);
            if (CheckTable.checkForExistence("DEPARTMENT")) {
            } else {
                throw new TableException("Таблица Department не создана");
            }

            CreateSomeTable.createOneOfTheTable("Organization", CreateSomeTable.CREATE_ORGANIZATION_TABLE);
            if (CheckTable.checkForExistence("ORGANIZATION")) {
            } else {
                throw new TableException("Таблица Organization не создана");
            }
        }
        catch (Exception e){
            logger.error("Не создана одна из таблиц");
        }
    }


    /**
     * Метод получения набора классов контроллеров
     */
    @Override
    public Set<Class<?>> getClasses() {
        final Set<Class<?>> classes = new HashSet<>();
        classes.add(PersonController.class);
        classes.add(DocumentController.class);
        return classes;
    }
}






