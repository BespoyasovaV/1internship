package ru.rest;
import ru.entity.document.Document;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.UUID;

/**
 * Контроллер, возвращает документы Person по ID
 * @author BaspoyasovaV
 */
@Path("/emp")
public class DocumentController {
    /**
     *Метод возвращает список документов по Id person-а
     */
    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_XML)
    public List<Document> getDocument(@PathParam("id") UUID id) {
        return DocumentService.getDocumentByAuthorId(id);
    }
}


