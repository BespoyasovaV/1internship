package ru.rest;

import ru.h2DB.PersonDatabase;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Контроллер, возвращает всех Person
 *
 * @author BespoyasovaV
 */
@Path("/employees")
public class PersonController {
    /**
     *Метод возвращает список Person-ов
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPerson() throws Exception {
        PersonDatabase h2dbPerson = PersonDatabase.getH2dbPerson();
        return Response.ok(h2dbPerson.read()).build();
    }
}
