package ru.rest;

import ru.entity.document.Document;
import ru.entity.staff.Person;
import ru.generate.PersonDocuments;
import ru.persons.single.PersonService;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

/**
 * @author BespoyasovaV
 * Класс для обработки документов
 */
public class DocumentService {
    /**
     * Метод  возвращает коллекцию документов по ID автора
     */
    public static List<Document> getDocumentByAuthorId(UUID id) {
        for (Person person : PersonService.getPerson()) {
            if (id.equals(person.getId())) {
                String fio = PersonNameUtils.personToString(person);
                return PersonDocuments.personDocuments.get(fio);
            }
        }
        return Collections.emptyList();
    }
}
