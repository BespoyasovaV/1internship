package ru.servlets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.servlets.facade.DocumentFacadeService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Сервлет выводит назнания документов на экран
 *
 * @author BespoyasovaV
 */
@WebServlet("/documents")
public class DocumentServlet extends HttpServlet {
    private static final Logger logger = LoggerFactory.getLogger(DocumentServlet.class);

    /**
     * Метод передает значения
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf-8");
        resp.getWriter();
        req = DocumentFacadeService.getDocumentFacade(req);
        RequestDispatcher dispatcher = req.getRequestDispatcher("WEB-INF/document.jsp");
        dispatcher.forward(req, resp);
    }
}
