package ru.servlets.facade;

import ru.entity.document.Document;
import ru.rest.DocumentController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.UUID;

/**
 * Класс-прослойка между сайтом и кодом
 *
 * @author BespoyasovaV
 */
public class DocumentFacadeService {
    /**
     * Метод устанавливает атрибуты req
     */
    public static HttpServletRequest getDocumentFacade(HttpServletRequest req) {
        DocumentController documentController = new DocumentController();
        UUID id = UUID.fromString(req.getParameter("userId"));
        List<Document> documentList = documentController.getDocument(id);
        for (Document doc : documentList) {
            req.setAttribute("name", doc.getName());
            req.setAttribute("id", doc.getId());
        }
        req.setAttribute("documentList", documentList);
        return req;
    }
}
