package ru.servlets.facade;

import ru.entity.staff.Person;
import ru.h2DB.PersonDatabase;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.UUID;

/**
 * Класс-прослойка между сайтом и кодом
 *
 * @author BespoyasovaV
 */
public class PersonFacadeService {
    /**
     * Метод устанавливает атрибуты req
     */
    public static HttpServletRequest getPersonFacade(HttpServletRequest req) {
        PersonDatabase personDatabase = PersonDatabase.getH2dbPerson();
        List<Person> personList = null;
        try {
            personList = personDatabase.readb();
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (Person person : personList) {
            UUID id = person.getId();
            req.setAttribute("id", id);
            String name = person.getName();
            req.setAttribute("name", name);
            String surname = person.getSurname();
            req.setAttribute("surname", surname);
            String patronymic = person.getPatronymic();
            req.setAttribute("patronymic", patronymic);
        }
        req.setAttribute("personList", personList);
        return req;
    }
}
