package ru.servlets.facade;

import ru.entity.document.Document;
import ru.generate.ProgramCollections;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.UUID;

/**
 * Класс-прослойка между сайтом и кодом
 *
 * @author BespoyasovaV
 */
public class DocumentFieldsFacadeService {
    /**
     * Метод устанавливает атрибуты req
     */
    public static HttpServletRequest getDocumentFieldsFacade(HttpServletRequest req) {
        List<Document> documentList = ProgramCollections.documents;
        UUID id = UUID.fromString(req.getParameter("documentId"));
        for (Document doc : documentList) {
            if (doc.getId().equals(id)) {
                req.setAttribute("idm", id);
                req.setAttribute("name", doc.getName());
                req.setAttribute("text", doc.getText());
                req.setAttribute("regNumber", doc.getRegNumber());
                req.setAttribute("registrationDate", doc.getRegistrationDate());
                req.setAttribute("document", doc);
                break;
            }
        }
        return req;
    }
}
