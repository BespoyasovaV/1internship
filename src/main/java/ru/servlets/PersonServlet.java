package ru.servlets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.servlets.facade.PersonFacadeService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Сервлет выводит person на экран
 *
 * @author BespoyasovaV
 */
public class PersonServlet extends HttpServlet {
    private static final Logger logger = LoggerFactory.getLogger(PersonServlet.class);

    /**
     * Метод передает значения
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf-8");
        resp.getWriter();
        req = PersonFacadeService.getPersonFacade(req);
        RequestDispatcher dispatcher = req.getRequestDispatcher("WEB-INF/person.jsp");
        dispatcher.forward(req, resp);
    }
}

