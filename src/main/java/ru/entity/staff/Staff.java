package ru.entity.staff;

import java.util.UUID;

/**
 * Класс родитель для оргштатных единиц
 *
 * @author BespoyasovaV
 */

public abstract class Staff {
    /**
     * ID оргштатной единицы
     */
    private UUID id;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

}
