package ru.entity.staff;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.UUID;

/**
 * Класс Организация
 *
 * @author BespoyasovaV
 */
@XmlRootElement(name = "Organization")
@XmlType(propOrder = {"id","phone","fullName", "shortName", "leader"})
public class Organization extends Staff {
    /**
     * полное наименование
     */
   private String fullName;
    /**
     * краткое наименование
     */
   private String shortName;
    /**
     * руководитель
     */
   private Person leader;
    /**
     * контактные телефоны
     */
   private String phone;

    @XmlElement(name = "id")
    public UUID getId() {
        return super.getId();
    }

    @XmlElement(name = "fullName")
    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @XmlElement(name = "shortName")
    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    @XmlElement(name = "leader")
    public Person getLeader() {
        return leader;
    }

    public void setLeader(Person leader) {
        this.leader = leader;
    }

    @XmlElement(name = "phone")
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Organization{" +
                "fullName='" + fullName + '\'' +
                ", shortName='" + shortName + '\'' +
                ", leader=" + leader +
                ", phone='" + phone + '\'' +
                '}';
    }

}
