package ru.entity.staff;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;
import java.util.UUID;

/**
 * Класс Сотрудник  для реализации "Элемнта организационной структуры"
 *
 * @author BespoyasovaV
 */
@XmlRootElement(name = "Person")
@XmlType(propOrder = {"id", "name", "surname", "patronymic", "post", "birthDay", "phoneNumber"})
public class Person extends Staff {
    /**
     * Имя
     */
    private String name;
    /**
     * Фамилия
     */
    private String surname;
    /**
     * Отчество
     */
    private String patronymic;
    /**
     * должность
     */
    private String post;
    /**
     * дата рождения
     */
    private Date birthDay;
    /**
     * номер телефона
     */
    private String phoneNumber;


    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public void setBirthDay(Date birthDay) {
        this.birthDay = birthDay;
    }

    public void setPhoneNumber(String telephoneNumber) {
        this.phoneNumber = telephoneNumber;
    }

    @XmlElement(name = "id")
    public UUID getId() {
        return super.getId();
    }

    @XmlElement(name = "name")
    public String getName() {
        return name;
    }

    @XmlElement(name = "surname")
    public String getSurname() {
        return surname;
    }

    @XmlElement(name = "patronymic")
    public String getPatronymic() {
        return patronymic;
    }

    @XmlElement(name = "post")
    public String getPost() {
        return post;
    }

    @XmlElement(name = "birthDay")
    @XmlSchemaType(name = "date")
    public Date getBirthDay() {
        return birthDay;
    }

    @XmlElement(name = "phoneNumber")
    public String getPhoneNumber() {
        return phoneNumber;
    }

    @Override
    public String toString() {
        return surname + '\'' + name + '\'' + patronymic;

    }
}
