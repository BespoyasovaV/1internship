package ru.entity.wrapper;

import ru.entity.staff.Staff;

import javax.xml.bind.annotation.XmlAnyElement;
import java.util.ArrayList;
import java.util.List;

/**
 * Класс-обёртка с листом, поможет читать .xml
 *
 * @author BespoyasovaV
 */
public class Wrapper<T extends Staff> {
    private List<T> items;

    public Wrapper() {
        items = new ArrayList<T>();
    }

    public Wrapper(List<T> items) {
        this.items = items;
    }

    @XmlAnyElement(lax = true)
    public List<T> getItems() {
        return items;
    }
}
