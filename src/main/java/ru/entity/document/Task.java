package ru.entity.document;

import ru.entity.staff.Person;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;
import java.util.UUID;

/**
 * Наследник класса Document, нужен для создания документов поручений
 *
 * @author BespoyasovaV
 */
@XmlRootElement(name = "Task")
@XmlType(propOrder = {"id","name","text","regNumber",
        "registrationDate","author","term","dateIssue","executorName",
        "sign","controller"})
public class Task extends Document {
    /**
     * Дата выдачи поручения
     */
    private Date dateIssue;
    /**
     * Срок исполнения пручения
     */
    private int term;
    /**
     * Ответственный исполнитель
     */
    private Person executorName;
    /**
     * Признак контрольности
     */
    private boolean sign;
    /**
     * Контролер поручения
     */
    private Boolean controller;
    @XmlElement(name = "id")
    public UUID getId() {
        return super.getId();
    }
    @XmlElement(name = "name")
    public String getName(){
        return super.getName();
    }
    @XmlElement(name = "text")
    public String getText(){
        return super.getText();
    }
    @XmlElement(name = "regNumber")
    public int getRegNumber(){
        return super.getRegNumber();
    }
    @XmlElement(name = "registrationDate")
    public Date getRegistrationDate(){
        return super.getRegistrationDate();
    }
    @XmlElement(name = "author")
    public Person getAuthor(){
        return super.getAuthor();
    }
    @XmlElement(name = "term")
    public int getTerm() {
        return term;
    }
    @XmlElement(name = "dateIssue")
    public Date getDateIssue() {
        return dateIssue;
    }
    @XmlElement(name = "executorName")
    public Person getExecutorName() {
        return executorName;
    }
    @XmlElement(name = "sign")
    public boolean getSign() {
        return sign;
    }
    @XmlElement(name = "controller")
    public boolean getController() {
        return controller;
    }

    public void setController(Boolean controller) {
        this.controller = controller;
    }

    public void setSign(Boolean sign) {
        this.sign = sign;
    }

    public void setExecutorName(Person executorName) {
        this.executorName = executorName;
    }

    public void setTerm(int term) {
        this.term = term;
    }

    public void setDateIssue(Date dateIssue) {
        this.dateIssue = dateIssue;
    }

    @Override
    public String toString() {
        return "Task{" +
                "dateIssue=" + dateIssue +
                ", term=" + term +
                ", executorName='" + executorName + '\'' +
                ", sign=" + sign +
                ", controller=" + controller +
                '}';
    }
}

