package ru.entity.document;

import ru.entity.staff.Person;
import ru.enums.Delivery;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;
import java.util.UUID;

/**
 * Наследник класса Document, нужен для создания исходящих документов
 *
 * @author BespoyasovaV
 */
@XmlRootElement(name = "Outgoing")
@XmlType(propOrder = {"id","name","text","regNumber",
        "registrationDate","author","destination","delivery"})
public class Outgoing extends Document {
    /**
     * Адресат
     */
    private Person destination;
    /**
     * Способ  доставки
     */
    private Delivery delivery;
    @XmlElement(name = "id")
    public UUID getId() {
        return super.getId();
    }
    @XmlElement(name = "name")
    public String getName(){
        return super.getName();
    }
    @XmlElement(name = "text")
    public String getText(){
        return super.getText();
    }
    @XmlElement(name = "regNumber")
    public int getRegNumber(){
        return super.getRegNumber();
    }
    @XmlElement(name = "registrationDate")
    public Date getRegistrationDate(){
        return super.getRegistrationDate();
    }
    @XmlElement(name = "author")
    public Person getAuthor(){
        return super.getAuthor();
    }
    @XmlElement(name = "destination")
    public Person getDestination() {
        return destination;
    }
    @XmlElement(name = "delivery")
    public Delivery getDelivery() {
        return delivery;
    }

    public void setDelivery(Delivery delivery) {
        this.delivery = delivery;
    }

    public void setDestination(Person destination) {
        this.destination = destination;
    }

    @Override
    public String toString() {
        return "Outgoing{" +
                "destination='" + destination + '\'' +
                ", delivery=" + delivery +
                '}';
    }

}
