package ru.entity.document;

import ru.entity.staff.Person;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;
import java.util.UUID;

/**
 * Наследник класса Document, нужен для создания входящих документов
 *
 * @author BespoyasovaV
 */
@XmlRootElement(name = "Incoming")
@XmlType(propOrder = {"id", "name", "text", "regNumber",
        "registrationDate", "author", "sender", "destination", "outgoingNumber",
        "outgoingDate"})
public class Incoming extends Document {
    /**
     * Отправитель
     */
    private Person sender;
    /**
     * Адресат
     */
    private Person destination;
    /**
     * Исходящий номер
     */
    private int outgoingNumber;
    /**
     * Исходящая дата регистрации
     */
    private Date outgoingDate;

    @XmlElement(name = "id")
    public UUID getId() {
        return super.getId();
    }

    @XmlElement(name = "name")
    public String getName() {
        return super.getName();
    }

    @XmlElement(name = "text")
    public String getText() {
        return super.getText();
    }

    @XmlElement(name = "regNumber")
    public int getRegNumber() {
        return super.getRegNumber();
    }

    @XmlElement(name = "registrationDate")
    public Date getRegistrationDate() {
        return super.getRegistrationDate();
    }

    @XmlElement(name = "author")
    public Person getAuthor() {
        return super.getAuthor();
    }

    @XmlElement(name = "sender")
    public Person getSender() {
        return sender;
    }

    public void setSender(Person sender) {
        this.sender = sender;
    }

    @XmlElement(name = "destination")
    public Person getDestination() {
        return destination;
    }

    public void setDestination(Person destination) {
        this.destination = destination;
    }

    @XmlElement(name = "outgoingNumber")
    public int getOutgoingNumber() {
        return outgoingNumber;
    }

    public void setOutgoingNumber(int outgoingNumber) {
        this.outgoingNumber = outgoingNumber;
    }

    @XmlElement(name = "outgoingDate")
    public Date getOutgoingDate() {
        return outgoingDate;
    }

    public void setOutgoingDate(Date outgoingDate) {
        this.outgoingDate = outgoingDate;
    }

    @Override
    public String toString() {
        return "Incoming{" +
                "sender='" + sender + '\'' +
                ", destination='" + destination + '\'' +
                ", outgoingNum=" + outgoingNumber +
                ", outgoingDate=" + outgoingDate +
                '}';
    }
}


