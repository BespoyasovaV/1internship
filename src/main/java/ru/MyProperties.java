package ru;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Класс для чтения из config.properties
 *
 * @author BespoyasovaV
 */
public class MyProperties {
    public static String filePath = "config.properties";
    private static final Logger logger = LoggerFactory.getLogger(MyProperties.class);

    /**
     * Метод принимает в параметри ключ и выводит значение
     */
    public static String getOutOfProperties(String attribute) {
        Properties properties = new Properties();
        try (InputStream inputStream = MyProperties.class.getClassLoader().getResourceAsStream(filePath)) {
            properties.load(inputStream);
        } catch (IOException e) {
            logger.error("Неверно прочитан файл");
        }
        return properties.getProperty(attribute);

    }
}
