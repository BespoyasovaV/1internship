import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.documents.factory.AbstractFactory;
import ru.documents.process.ProcessingDocuments;
import ru.entity.document.Document;
import ru.exceptions.DocumentExistException;
import ru.generate.ProgramCollections;
import ru.generate.GenerateRandomFactory;
import ru.documents.factory.DocumentCreate;
import ru.generate.RandomValueService;
import ru.gson.gsonBuild;

import java.io.IOException;


/**
 * Класс для запуска приложения, в консоль выводится список документов
 *
 * @author BespoyasovaV
 */
public class MainClass {
    private static final Logger logger = LoggerFactory.getLogger(AbstractFactory.class);
    private static final int NUMBER_OF_DOCUMENTS = 180;

    public static void main(String[] args) throws IOException {
        try {
            int randomValueForDocList = RandomValueService.generateRandomNumber(NUMBER_OF_DOCUMENTS);
            for (int i = 0; i < randomValueForDocList; i++) {
                DocumentCreate documentCreate = GenerateRandomFactory.generateRandomFactory();
                Document document = documentCreate.create();
                ProgramCollections.documents.add(document);
            }

        } catch (DocumentExistException e) {
            logger.error("Документ с генерируемым номером уже есть");
        }
        ProcessingDocuments.populate();
        gsonBuild.writeJson();
    }
}
